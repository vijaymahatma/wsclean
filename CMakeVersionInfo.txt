set(WSCLEAN_VERSION_STR   3.4)
set(WSCLEAN_VERSION_MAJOR 3)
set(WSCLEAN_VERSION_MINOR 4)
set(WSCLEAN_VERSION_DATE  2023-10-11)

# SOVERSION stored in the library -- increase
# when making ABI changes
set(WSCLEAN_VERSION_SO    2)

# Get the latest abbreviated commit hash of the working branch
execute_process(
  COMMAND git describe
  WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
  OUTPUT_VARIABLE WSCLEAN_GIT_HASH
  OUTPUT_STRIP_TRAILING_WHITESPACE)